module Data.Graphics.Pixi.Container(
    PixiAddChild(..)
  , PixiRemoveChildren(..)
  , MonadJSM
  ) where

import Language.Javascript.JSaddle

-- | Attach graphics context to the PIXI element
class PixiAddChild a b where
  pixiAddChild :: MonadJSM m
    => a -- ^ Where to add
    -> b -- ^ Whom to add
    -> m ()

class PixiRemoveChildren a where
  pixiRemoveChildren :: MonadJSM m => a -> m ()
