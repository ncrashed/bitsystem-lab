module Data.Graphics.Pixi.App(
    PixiApp(..)
  , pixiNewApp
  , pixiNewAppResizeable
  , pixiAppResizeToParent
  , pixiAppScreenWidth
  , pixiAppScreenHeight
  , pixiAppendTo
  , pixiStageInteractive
  , pixiSetOnClick
  , pixiAddTicker
  , pixiOnResize
  , pixiSetScale
  , PixiAddChild(..)
  ) where

import Control.Monad.IO.Class
import Data.Graphics.Pixi.Container
import Data.Graphics.Pixi.Graphics
import Data.Graphics.Pixi.Text
import Language.Javascript.JSaddle

#ifdef ghcjs_HOST_OS
import GHCJS.Foreign.Callback
#endif

-- | Handle to the main app of Pixi
newtype PixiApp = PixiApp { unPixiApp :: JSVal }

-- | Create new PIXI element with drawing context
pixiNewApp :: MonadJSM m
  => Int -- ^ Width
  -> Int -- ^ Height
  -> Color -- ^ Background color
  -> m PixiApp

-- | Create new PIXI element with drawing context
pixiNewAppResizeable :: MonadJSM m
  => Color -- ^ Background color
  -> m PixiApp

-- | Resize PIXI element to parent size
pixiAppResizeToParent :: MonadJSM m
  => PixiApp
  -> m ()

-- | Get viewport width of pixi app
pixiAppScreenWidth :: MonadJSM m
  => PixiApp
  -> m Int

-- | Get viewport height of pixi app
pixiAppScreenHeight :: MonadJSM m
  => PixiApp
  -> m Int

-- | Attach PIXI element to the given dom element
pixiAppendTo :: MonadJSM m
  => PixiApp
  -> JSVal
  -> m ()

-- | Enable interactive events on PIXI element
pixiStageInteractive :: MonadJSM m
  => PixiApp
  -> Bool
  -> m ()

-- | Call function on mouse click
pixiSetOnClick :: MonadJSM m
  => PixiApp
  -> JSM ()
  -> m ()

-- | Call function on each draw tick
pixiAddTicker :: MonadJSM m
  => PixiApp
  -> JSM ()
  -> m ()

-- | Call function on resize of pixi app, resizes pixi app to parent
pixiOnResize :: MonadJSM m
  => PixiApp
  -> JSM ()
  -> m ()

-- | Resize content of renderer by given factor per x and y
pixiSetScale :: MonadJSM m
  => PixiApp
  -> X
  -> Y
  -> m ()

#ifdef ghcjs_HOST_OS

foreign import javascript safe "new PIXI.Application($1, $2, { antialias: true, transparent: true, backgroundColor: $3, resolution: devicePixelRatio })"
  js_pixi_new_app :: Int -> Int -> Color -> IO JSVal

pixiNewApp w h c = fmap PixiApp $ liftIO $ js_pixi_new_app w h c
{-# INLINABLE pixiNewApp #-}

foreign import javascript safe "new PIXI.Application({ antialias: true, backgroundColor: $1, autoResize: true, resolution: devicePixelRatio })"
  js_pixi_new_app_resizeable :: Color -> IO JSVal

pixiNewAppResizeable c = fmap PixiApp $ liftIO $ js_pixi_new_app_resizeable c
{-# INLINABLE pixiNewAppResizeable #-}

foreign import javascript safe "{ var parent = $1.view.parentNode; $1.renderer.resize(parent.clientWidth, parent.clientHeight); }"
  js_pixi_resize_to_parent :: JSVal -> IO ()

pixiAppResizeToParent (PixiApp a) = liftIO $ js_pixi_resize_to_parent a
{-# INLINABLE pixiAppResizeToParent #-}

foreign import javascript safe "$1.screen.width"
  js_pixi_screen_width :: JSVal -> IO Int

pixiAppScreenWidth (PixiApp a) = liftIO $ js_pixi_screen_width a
{-# INLINABLE pixiAppScreenWidth #-}

foreign import javascript safe "$1.screen.height"
  js_pixi_screen_height :: JSVal -> IO Int

pixiAppScreenHeight (PixiApp a) = liftIO $ js_pixi_screen_height a
{-# INLINABLE pixiAppScreenHeight #-}

foreign import javascript safe "$2.appendChild($1.view);"
  js_pixi_append_to :: JSVal -> JSVal -> IO ()

pixiAppendTo (PixiApp a) e = liftIO $ js_pixi_append_to a e
{-# INLINABLE pixiAppendTo #-}

foreign import javascript safe "$1.stage.interactive = $2;"
  js_pixi_stage_interactive :: JSVal -> Bool -> IO ()

pixiStageInteractive (PixiApp a) v = liftIO $ js_pixi_stage_interactive a v
{-# INLINABLE pixiStageInteractive #-}

foreign import javascript safe "$1.stage.addChild($2);"
  js_pixi_add_child :: JSVal -> JSVal -> IO ()

instance PixiAddChild PixiApp PixiGraphics where
  pixiAddChild (PixiApp a) (PixiGraphics v) = liftIO $ js_pixi_add_child a v
  {-# INLINABLE pixiAddChild #-}

instance PixiAddChild PixiApp PixiText where
  pixiAddChild (PixiApp a) (PixiText v) = liftIO $ js_pixi_add_child a v
  {-# INLINABLE pixiAddChild #-}

foreign import javascript safe "$1.stage.on('pointertap', $2);"
  js_pixi_set_on_click :: JSVal -> Callback (IO ()) -> IO ()

pixiSetOnClick (PixiApp a) f = liftIO $ do
  cb <- asyncCallback f
  js_pixi_set_on_click a cb
{-# INLINABLE pixiSetOnClick #-}

foreign import javascript safe "$1.ticker.add($2);"
  js_pixi_add_ticker :: JSVal -> Callback (IO ()) -> IO ()

pixiAddTicker (PixiApp a) f = liftIO $ do
  cb <- asyncCallback f
  js_pixi_add_ticker a cb
{-# INLINABLE pixiAddTicker #-}

foreign import javascript safe "window.addEventListener('resize',$1);"
  js_pixi_on_resize :: Callback (IO ()) -> IO ()

pixiOnResize app f = liftIO $ do
  cb <- asyncCallback $ do
    pixiAppResizeToParent app
    f
  js_pixi_on_resize cb

foreign import javascript safe "$1.stage.scale.set($2, $3);"
  js_pixi_set_scale :: JSVal -> X -> Y -> IO ()

pixiSetScale (PixiApp a) x y = liftIO $ js_pixi_set_scale a x y

#else

pixiNewApp _ _ _ = error "pixiNewApp: unimplemented"

pixiNewAppResizeable = error "pixiNewAppResizeable: unimplemented"

pixiAppResizeToParent = error "pixiAppResizeToParent: unimplemented"

pixiAppScreenWidth = error "pixiAppScreenWidth: unimplemented"

pixiAppScreenHeight = error "pixiAppScreenHeight: unimplemented"

pixiAppendTo = error "pixiAppendTo: unimplemented"

pixiStageInteractive = error "pixiStageInteractive: unimplemented"

instance PixiAddChild PixiApp PixiGraphics where
  pixiAddChild = error "pixiAddChild: unimplemented"

instance PixiAddChild PixiApp PixiText where
  pixiAddChild = error "pixiAddChild: unimplemented"

pixiSetOnClick = error "pixiSetOnClick: unimplemented"

pixiAddTicker = error "pixiAddTicker: unimplemented"

pixiOnResize = error "pixiOnResize: unimplemented"

pixiSetScale = error "pixiSetScale: unimplemented"

#endif
