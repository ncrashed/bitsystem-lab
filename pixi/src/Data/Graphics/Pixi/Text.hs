module Data.Graphics.Pixi.Text(
    PixiText(..)
  , PixiTextConf(..)
  , newPixiText
  , textSetX
  , textSetY
  , textSetText
  , textSetAnchor
  , textMetrics
  ) where

import Control.Monad.IO.Class
import Data.Aeson
import Data.Default
import Data.Graphics.Pixi.Container
import Data.Graphics.Pixi.Graphics
import Data.Text (Text)
import GHC.Generics
import Language.Javascript.JSaddle

#ifdef ghcjs_HOST_OS
import GHCJS.Foreign.Callback
#endif

-- | Handle to text object of pixi
newtype PixiText = PixiText { unPixiText :: JSVal }

-- | Style configuration of 'PixiText'
data PixiTextConf = PixiTextConf {
  ptextFontFamily   :: !Text
, ptextFontSize     :: !Float
, ptextFontStyle    :: !Text
, ptextFontVariant  :: !Text
, ptextFontWeight   :: !Text
, ptextFill         :: !Color
, ptextAlign        :: !Text
} deriving (Eq, Show, Generic)

instance Default PixiTextConf where
  def = PixiTextConf {
      ptextFontFamily     = "Arial"
    , ptextFontSize       = 18
    , ptextFontStyle      = "normal"
    , ptextFontVariant    = "normal"
    , ptextFontWeight     = "normal"
    , ptextFill           = 0x868a8d
    , ptextAlign          = "center"
    }
  {-# INLINE def #-}

instance ToJSON PixiTextConf where
  toJSON PixiTextConf{..} = object [
      "fontFamily"  .= ptextFontFamily
    , "fontSize"    .= ptextFontSize
    , "fontStyle"   .= ptextFontStyle
    , "fontVariant" .= ptextFontVariant
    , "fontWeight"  .= ptextFontWeight
    , "fill"        .= ptextFill
    , "align"       .= ptextAlign
    ]
  {-# INLINE toJSON #-}

instance ToJSVal PixiTextConf where
  toJSVal = toJSVal . toJSON
  {-# INLINE toJSVal #-}

newPixiText :: MonadJSM m => Text -> PixiTextConf -> m PixiText
{-# INLINEABLE newPixiText #-}

-- | Set x component of position for text node
textSetX :: MonadJSM m => PixiText -> X -> m ()
{-# INLINABLE textSetX #-}

-- | Set y component of position for text node
textSetY :: MonadJSM m => PixiText -> Y -> m ()
{-# INLINABLE textSetY #-}

-- | Update text, automatically recreates internal texture if needed
textSetText :: MonadJSM m => PixiText -> Text -> m ()
{-# INLINABLE textSetText #-}

textSetAnchor :: MonadJSM m => PixiText -> X -> Y -> m ()
{-# INLINABLE  textSetAnchor #-}

-- | Calculate text size (width and height)
textMetrics :: MonadJSM m => Text -> PixiTextConf -> m (X, Y)
{-# INLINABLE textMetrics #-}

#ifdef ghcjs_HOST_OS

foreign import javascript safe "new PIXI.Text($1, $2)"
  js_pixi_new_text :: Text -> JSVal -> IO JSVal

newPixiText t c = liftIO $ do
  v <- toJSVal c
  PixiText <$> js_pixi_new_text t v

foreign import javascript safe "$1.x = $2;"
  js_pixi_text_set_x :: JSVal -> X -> IO ()

textSetX (PixiText g) x = liftIO $ js_pixi_text_set_x g x

foreign import javascript safe "$1.y = $2;"
  js_pixi_text_set_y :: JSVal -> X -> IO ()

textSetY (PixiText g) y = liftIO $ js_pixi_text_set_y g y

foreign import javascript safe "$1.text = $2;"
  js_pixi_text_set_text :: JSVal -> Text -> IO ()

textSetText (PixiText g) t = liftIO $ js_pixi_text_set_text g t

foreign import javascript safe "$1.anchor.set($2, $3);"
  js_pixi_text_set_anchor :: JSVal -> X -> Y -> IO ()

textSetAnchor (PixiText g) x y = liftIO $ js_pixi_text_set_anchor g x y

foreign import javascript safe "$1.addChild($2);"
  js_pixi_add_child :: JSVal -> JSVal -> IO ()

instance PixiAddChild PixiGraphics PixiText where
  pixiAddChild (PixiGraphics a) (PixiText v) = liftIO $ js_pixi_add_child a v
  {-# INLINABLE pixiAddChild #-}

foreign import javascript safe "PIXI.TextMetrics.measureText($1, new PIXI.TextStyle($2))"
  js_pixi_measure_text :: Text -> JSVal -> IO JSVal

foreign import javascript safe "$1.width"
  js_pixi_text_metrics_width :: JSVal -> IO Float

foreign import javascript safe "$1.height"
  js_pixi_text_metrics_height :: JSVal -> IO Float

textMetrics t s = liftIO $ do
  sv <- toJSVal s
  metrics <- js_pixi_measure_text t sv
  (,) <$> js_pixi_text_metrics_width metrics <*> js_pixi_text_metrics_height metrics

#else

newPixiText = error "newPixiText: unimplemented"
textSetX = error "textSetX: unimplemented"
textSetY = error "textSetY: unimplemented"
textSetText = error "textSetText: unimplemented"
textSetAnchor = error "textSetAnchor: unimplemented"
textMetrics = error "textMetrics: unimplemented"

instance PixiAddChild PixiGraphics PixiText where
  pixiAddChild = error "pixiAddChild: unimplemented"

#endif
