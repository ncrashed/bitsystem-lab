module Data.Graphics.Pixi.Graphics(
    Color
  , Alpha
  , Width
  , Height
  , Radius
  , X
  , Y
  , Rotation
  , PixiGraphics(..)
  , newGraphics
  , graphicsSetX
  , graphicsSetY
  , graphicsSetRotation
  , graphicsClear
  , beginFill
  , endFill
  , lineStyle
  , moveTo
  , lineTo
  , closePath
  , drawRect
  , drawCircle
  , bezierCurveTo
  , quadraticCurveTo
  , graphicsSetOnClick
  , graphicsSetOnPointerDown
  , graphicsSetOnPointerUp
  , graphicsSetOnPointerUpOutside
  , graphicsSetInteractive
  , graphicsDestroy
  ) where

import Control.Monad.IO.Class
import Data.Graphics.Pixi.Container
import Language.Javascript.JSaddle

#ifdef ghcjs_HOST_OS
import GHCJS.Foreign.Callback
#endif

-- | Hex defined color
type Color = Int

-- | Alpha color value
type Alpha = Float

-- | Width units
type Width = Float

-- | Width units
type Height = Float

-- | Width units
type Radius = Float

-- | X coordinate
type X = Float

-- | Y coordinate
type Y = Float

-- | Angle units in radians
type Rotation = Float

-- | Handle to the main app of Pixi
newtype PixiGraphics = PixiGraphics { unPixiGraphics :: JSVal }

-- | Create new PIXI element with drawing context
newGraphics :: MonadJSM m => m PixiGraphics
-- | Set x component of position for graphics node
graphicsSetX :: MonadJSM m => PixiGraphics -> X -> m ()
-- | Set y component of position for graphics node
graphicsSetY :: MonadJSM m => PixiGraphics -> Y -> m ()
-- | Set rotation around axis in radians
graphicsSetRotation :: MonadJSM m => PixiGraphics -> Rotation -> m ()
-- | Clear all drawing context
graphicsClear :: MonadJSM m => PixiGraphics -> m ()
-- | Start drawing filled figure with given graphics context
beginFill :: MonadJSM m => PixiGraphics -> Color -> Alpha -> m ()
-- | Finish drawing filled figure with given graphics context
endFill :: MonadJSM m => PixiGraphics -> m ()
-- | Set line width color and alpha for given context
lineStyle :: MonadJSM m => PixiGraphics -> Width -> Color -> Alpha -> m ()
-- | Move current cursor of drawing context to the point
moveTo :: MonadJSM m => PixiGraphics -> X -> Y -> m ()
-- | Draw line from current cursor of drawing context to the point
lineTo :: MonadJSM m => PixiGraphics -> X -> Y -> m ()
-- | Close figure with line from end to begin
closePath :: MonadJSM m => PixiGraphics -> m ()
-- | Draw rectangular with the current context
drawRect :: MonadJSM m => PixiGraphics -> X -> Y -> Width -> Height -> m ()
-- | Draw circle with the current context
drawCircle :: MonadJSM m => PixiGraphics -> X -> Y -> Radius -> m ()

-- | Calculate the points for a quadratic bezier curve and then draws it. Based on: https://stackoverflow.com/questions/785097/how-do-i-implement-a-bezier-curve-in-c
quadraticCurveTo :: MonadJSM m => PixiGraphics
  -> X -- ^ Control point
  -> Y -- ^ Control point
  -> X -- ^ Desitnation point
  -> Y -- ^ Destination point
  -> m ()

-- | Calculate the points for a bezier curve and then draws it.
bezierCurveTo :: MonadJSM m => PixiGraphics
  -> X -- ^ First control point
  -> Y -- ^ First control point
  -> X -- ^ Second control point
  -> Y -- ^ Second control point
  -> X -- ^ Dist point
  -> Y -- ^ Dist point
  -> m ()

-- | Call function on mouse click
graphicsSetOnClick :: MonadJSM m
  => PixiGraphics
  -> JSM ()
  -> m ()

-- | Call function on mouse button release inside of object
graphicsSetOnPointerDown :: MonadJSM m
  => PixiGraphics
  -> JSM ()
  -> m ()

-- | Call function on mouse button release inside of object
graphicsSetOnPointerUp :: MonadJSM m
  => PixiGraphics
  -> JSM ()
  -> m ()

-- | Call function on mouse button release outside of object
graphicsSetOnPointerUpOutside :: MonadJSM m
  => PixiGraphics
  -> JSM ()
  -> m ()

-- | Enable events for inputs for the graphics
graphicsSetInteractive :: MonadJSM m
  => PixiGraphics
  -> Bool
  -> m ()

-- | Destroys the Graphics object.
graphicsDestroy :: MonadJSM m
  => PixiGraphics
  -> m ()

#ifdef ghcjs_HOST_OS

foreign import javascript safe "new PIXI.Graphics()"
  js_pixi_new_graphics :: IO JSVal

newGraphics = fmap PixiGraphics $ liftIO js_pixi_new_graphics
{-# INLINABLE newGraphics #-}

foreign import javascript safe "$1.x = $2;"
  js_pixi_graphics_set_x :: JSVal -> X -> IO ()

graphicsSetX (PixiGraphics g) x = liftIO $ js_pixi_graphics_set_x g x
{-# INLINABLE graphicsSetX #-}

foreign import javascript safe "$1.y = $2;"
  js_pixi_graphics_set_y :: JSVal -> X -> IO ()

graphicsSetY (PixiGraphics g) y = liftIO $ js_pixi_graphics_set_y g y
{-# INLINABLE graphicsSetY #-}

foreign import javascript safe "$1.rotation = $2;"
  js_pixi_graphics_set_rotation :: JSVal -> Rotation -> IO ()

graphicsSetRotation (PixiGraphics g) r = liftIO $ js_pixi_graphics_set_rotation g r
{-# INLINABLE graphicsSetRotation #-}

foreign import javascript safe "$1.clear();"
  js_pixi_graphics_clear :: JSVal-> IO ()

graphicsClear (PixiGraphics g) = liftIO $ js_pixi_graphics_clear g
{-# INLINABLE graphicsClear #-}

foreign import javascript safe "$1.beginFill($2, $3);"
  js_pixi_begin_fill :: JSVal -> Color -> Alpha -> IO ()

beginFill (PixiGraphics g) v alpha = liftIO $ js_pixi_begin_fill g v alpha
{-# INLINABLE beginFill #-}

foreign import javascript safe "$1.endFill();"
  js_pixi_end_fill :: JSVal -> IO ()

endFill (PixiGraphics g) = liftIO $ js_pixi_end_fill g
{-# INLINABLE endFill #-}

foreign import javascript safe "$1.lineStyle($2, $3, $4);"
  js_pixi_line_style :: JSVal -> Width -> Color -> Alpha -> IO ()

lineStyle (PixiGraphics g) w c a = liftIO $ js_pixi_line_style g w c a
{-# INLINABLE lineStyle #-}

foreign import javascript safe "$1.moveTo($2, $3);"
  js_pixi_move_to :: JSVal -> X -> Y -> IO ()

moveTo (PixiGraphics g) x y = liftIO $ js_pixi_move_to g x y
{-# INLINABLE moveTo #-}

foreign import javascript safe "$1.lineTo($2, $3);"
  js_pixi_line_to :: JSVal -> X -> Y -> IO ()

lineTo (PixiGraphics g) x y = liftIO $ js_pixi_line_to g x y
{-# INLINABLE lineTo #-}

foreign import javascript safe "$1.closePath();"
  js_pixi_close_path :: JSVal -> IO ()

closePath (PixiGraphics g) = liftIO $ js_pixi_close_path g
{-# INLINABLE closePath #-}

foreign import javascript safe "$1.drawRect($2, $3, $4, $5);"
  js_pixi_draw_rect :: JSVal -> X -> Y -> Width -> Height -> IO ()

drawRect (PixiGraphics g) x y w h = liftIO $ js_pixi_draw_rect g x y w h
{-# INLINABLE drawRect #-}

foreign import javascript safe "$1.drawCircle($2, $3, $4);"
  js_pixi_draw_circle :: JSVal -> X -> Y -> Radius -> IO ()

drawCircle (PixiGraphics g) x y r = liftIO $ js_pixi_draw_circle g x y r
{-# INLINABLE drawCircle #-}

foreign import javascript safe "$1.quadraticCurveTo($2, $3, $4, $5);"
  js_pixi_quadratic_curve_to :: JSVal -> X -> Y -> X -> Y -> IO ()

quadraticCurveTo (PixiGraphics g) cpx cpy dx dy = liftIO $ js_pixi_quadratic_curve_to g cpx cpy dx dy
{-# INLINABLE quadraticCurveTo #-}

foreign import javascript safe "$1.bezierCurveTo($2, $3, $4, $5, $6, $7);"
  js_pixi_bezier_curve_to :: JSVal -> X -> Y -> X -> Y -> X -> Y -> IO ()

bezierCurveTo (PixiGraphics g) cpX cpY cpX2 cpY2 toX toY = liftIO $ js_pixi_bezier_curve_to g cpX cpY cpX2 cpY2 toX toY
{-# INLINABLE bezierCurveTo #-}

foreign import javascript safe "$1.on('pointertap', $2);"
  js_pixi_graphics_set_on_click :: JSVal -> Callback (IO ()) -> IO ()

graphicsSetOnClick (PixiGraphics a) f = liftIO $ do
  cb <- asyncCallback f
  js_pixi_graphics_set_on_click a cb
{-# INLINABLE graphicsSetOnClick #-}

foreign import javascript safe "$1.on('pointerdown', $2);"
  js_pixi_graphics_set_on_pointer_down :: JSVal -> Callback (IO ()) -> IO ()

graphicsSetOnPointerDown (PixiGraphics a) f = liftIO $ do
  cb <- asyncCallback f
  js_pixi_graphics_set_on_pointer_down a cb
{-# INLINABLE graphicsSetOnPointerDown #-}

foreign import javascript safe "$1.on('pointerup', $2);"
  js_pixi_graphics_set_on_pointer_up :: JSVal -> Callback (IO ()) -> IO ()

graphicsSetOnPointerUp (PixiGraphics a) f = liftIO $ do
  cb <- asyncCallback f
  js_pixi_graphics_set_on_pointer_up a cb
{-# INLINABLE graphicsSetOnPointerUp #-}

foreign import javascript safe "$1.on('pointerupoutside', $2);"
  js_pixi_graphics_set_on_pointer_up_outside :: JSVal -> Callback (IO ()) -> IO ()

graphicsSetOnPointerUpOutside (PixiGraphics a) f = liftIO $ do
  cb <- asyncCallback f
  js_pixi_graphics_set_on_pointer_up_outside a cb
{-# INLINABLE graphicsSetOnPointerUpOutside #-}

foreign import javascript safe "$1.interactive = $2;"
  js_pixi_graphics_interactive :: JSVal -> Bool -> IO ()

graphicsSetInteractive  (PixiGraphics g) v = liftIO $ do
  js_pixi_graphics_interactive g v
{-# INLINABLE graphicsSetInteractive #-}

foreign import javascript safe "$1.destroy();"
  js_pixi_graphics_destroy :: JSVal -> IO ()

graphicsDestroy (PixiGraphics g) = liftIO $ js_pixi_graphics_destroy g
{-# INLINEABLE graphicsDestroy #-}

foreign import javascript safe "$1.addChild($2);"
  js_pixi_add_child :: JSVal -> JSVal -> IO ()

instance PixiAddChild PixiGraphics PixiGraphics where
  pixiAddChild (PixiGraphics a) (PixiGraphics v) = liftIO $ js_pixi_add_child a v
  {-# INLINABLE pixiAddChild #-}

foreign import javascript safe "$1.removeChildren();"
  js_pixi_remove_children :: JSVal -> IO ()

instance PixiRemoveChildren PixiGraphics where
  pixiRemoveChildren (PixiGraphics a) = liftIO $ js_pixi_remove_children a
  {-# INLINABLE pixiRemoveChildren #-}

#else


newGraphics = error "newGraphics: unimplemented"

graphicsSetX = error "graphicsSetX: unimplemented"

graphicsSetY = error "graphicsSetY: unimplemented"

graphicsSetRotation = error "graphicsSetRotation: unimplemented"

graphicsClear = error "graphicsClear: unimplemented"

beginFill = error "beginFill: unimplemented"

endFill = error "endFill: unimplemented"

lineStyle = error "lineStyle: unimplemented"

moveTo = error "moveTo: unimplemented"

lineTo = error "lineTo: unimplemented"

closePath = error "closePath: unimplemented"

drawRect = error "drawRect: unimplemented"

drawCircle = error "drawCircle: unimplemented"

bezierCurveTo = error "bezierCurveTo: unimplemented"

quadraticCurveTo = error "quadraticCurveTo: unimplemented"

graphicsSetOnClick = error "graphicsSetOnClick: unimplemented"

graphicsSetOnPointerDown = error "graphicsSetOnPointerDown: unimplemented"

graphicsSetOnPointerUp = error "graphicsSetOnPointerUp: unimplemented"

graphicsSetOnPointerUpOutside = error "graphicsSetOnPointerUpOutside: unimplemented"

graphicsSetInteractive = error "graphicsSetInteractive: unimplemented"

graphicsDestroy = error "graphicsDestroy: unimplemented"

instance PixiAddChild PixiGraphics PixiGraphics where
  pixiAddChild = error "pixiAddChild: unimplemented"

instance PixiRemoveChildren PixiGraphics where
  pixiRemoveChildren = error "pixiRemoveChildren: unimplemented"

#endif
