module Data.Graphics.Pixi.Interaction(
    PixiInteraction(..)
  , getPixiInteraction
  , getMouseX
  , getMouseY
  ) where

import Control.Monad.IO.Class
import Data.Graphics.Pixi.App
import Language.Javascript.JSaddle

#ifdef ghcjs_HOST_OS
import GHCJS.Foreign.Callback
#endif

newtype PixiInteraction = PixiInteraction { unPixiInteraction :: JSVal }

getPixiInteraction :: MonadJSM m => PixiApp -> m PixiInteraction
{-# INLINABLE getPixiInteraction #-}

getMouseX :: MonadJSM m => PixiInteraction -> m Float
{-# INLINABLE getMouseX #-}

getMouseY :: MonadJSM m => PixiInteraction -> m Float
{-# INLINABLE getMouseY #-}

#ifdef ghcjs_HOST_OS

foreign import javascript safe "$1.renderer.plugins.interaction"
  js_pixi_get_interaction :: JSVal -> IO JSVal

getPixiInteraction (PixiApp app) = liftIO $ do
  fmap PixiInteraction $ js_pixi_get_interaction app

foreign import javascript safe "$1.mouse.global.x"
  js_pixi_get_mouse_x :: JSVal -> IO Float
foreign import javascript safe "$1.mouse.global.y"
  js_pixi_get_mouse_y :: JSVal -> IO Float

getMouseX (PixiInteraction v) = liftIO $ js_pixi_get_mouse_x v
getMouseY (PixiInteraction v) = liftIO $ js_pixi_get_mouse_y v
#else

getPixiInteraction = error "getPixiInteraction: unimplemented"
getMouseX = error "getMouseX: unimplemented"
getMouseY = error "getMouseY: unimplemented"

#endif
