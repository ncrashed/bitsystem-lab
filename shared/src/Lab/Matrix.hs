{-# LANGUAGE DeriveTraversable #-}
module Lab.Matrix where

import Data.Vector (Vector)

import qualified Data.Vector as V

-- | Count of rows
type Rows = Int
-- | Count of columns
type Columns = Int

-- | Row index 0 started
type Row = Int
-- | Column index 0 started
type Column = Int

-- | Flat index of data in array 0 started
type Index = Int

data Matrix a = Matrix {
    matrixRows    :: !Rows
  , matrixColumns :: !Columns
  , matrixData    :: !(Vector a)
  } deriving (Show, Functor, Foldable, Traversable)

makeMatrix ::
     Rows -- ^ Row count
  -> Columns -- ^ Column count
  -> (Row -> Column -> a) -- ^ Value setting
  -> Matrix a
makeMatrix rows columns f = Matrix rows columns datum
  where
    datum = V.generate (rows*columns) $ uncurry f . fromIndex columns

emptyMatrix :: Matrix a
emptyMatrix = Matrix 0 0 V.empty

-- | Make matrix from rows of elements. Expected to have same width of each vector
matrixFromRows :: Vector (Vector a) -> Matrix a
matrixFromRows vva
  | V.null vva = emptyMatrix
  | otherwise  = Matrix rows columns (V.concat . V.toList $ vva)
  where
    rows = V.length vva
    columns = V.length (V.head vva)

-- | Convert matrix to rows
matrixToRows :: Matrix a -> Vector (Vector a)
matrixToRows m = go V.empty $ matrixData m
  where
    n = matrixColumns m
    go acc datum
      | V.length datum <= n = V.snoc acc datum
      | otherwise = go (V.snoc acc (V.take n datum)) $ V.drop n datum

-- | Convert flat index to two coordinates (row, column)
fromIndex :: Columns -- ^ Coloumns count
  -> Index -- ^ Flat index
  -> (Row, Column) -- ^ Row and column index
fromIndex columns i = (i `div` columns, i `mod` columns)

-- | Convert row and column indecies to single flat index
toIndex :: Columns  -- ^ Coloumns count
  -> Row -- ^ Row index
  -> Column -- ^ Column index
  -> Index -- ^ Flat index
toIndex columns ri ci = ci + ri * columns

-- | Left fold over matrix with indecies. Strict on accumulator.
foldMatrix :: (Row -> Column -> a -> b -> a) -- ^ Folding function
  -> a -- ^ Initial accumulator value
  -> Matrix b -- ^ Matrix to fold over
  -> a -- ^ Resulted accumulator
foldMatrix f a0 m = V.ifoldl' f' a0 $ matrixData m
  where
    f' !acc !i !b = let
      (ri, ci) = fromIndex (matrixColumns m) i
      in f ri ci acc b

-- | Traverse matrix and perform action per each row
forMRows :: Monad m
  => Matrix a
  -> (Int -> Vector a -> m (Vector b))
  -> m (Matrix b)
forMRows m f = fmap matrixFromRows $ V.imapM f $ matrixToRows m

matrixAt :: Matrix a
  -> Int
  -> Int
  -> Maybe a
matrixAt m i j = matrixData m V.!? toIndex (matrixColumns m) i j

matrixAtUnsafe :: Matrix a
  -> Int
  -> Int
  -> a
matrixAtUnsafe m i j = matrixData m V.! toIndex (matrixColumns m) i j

matrixSet :: Int -> Int -> a -> Matrix a -> Matrix a
matrixSet i j a m = m { matrixData = matrixData m V.// [(k, a)] }
  where
    k = toIndex (matrixColumns m) i j
