{-# LANGUAGE MultiWayIf #-}
module Lab.Graph where

import Control.Monad (join, (<=<))
import Data.Bifunctor
import Data.List (foldl')
import Data.List.NonEmpty (NonEmpty(..))
import Data.Maybe
import Data.Vector (Vector)
import Lab.Matrix

import qualified Data.Vector as V
import qualified Data.IntSet as IS
import qualified Data.List.NonEmpty as NE

type NodeId = Int

data Graph a = Graph {
    graphNodes :: !(Vector a)
  , graphEdges :: !(Vector (GraphEdge a))
  } deriving (Show, Functor, Foldable, Traversable)

emptyGraph :: Graph a
emptyGraph = Graph V.empty V.empty

hasEdge :: NodeId -> NodeId -> Graph a -> Bool
hasEdge i j = isJust . V.findIndex (\(GraphEdge i' j' _) -> i' == i && j == j') . graphEdges

data GraphEdge a = GraphEdge !NodeId !NodeId !a
  deriving (Show, Functor, Foldable, Traversable)

edgeData :: GraphEdge a -> a
edgeData (GraphEdge _ _ a) =  a

graphVerts :: Graph a -> Int
graphVerts = V.length . graphNodes

getEdge :: NodeId -> NodeId -> Graph (EdgeNode e n) -> Maybe e
getEdge i j = join . fmap (asEdge . edgeData) . V.find f . graphEdges
  where
    f (GraphEdge i' j' (JustEdge e)) = i' == i && j == j'
    f (GraphEdge _ _ (JustNode _)) = False

getNode :: NodeId -> Graph (EdgeNode e n) -> Maybe n
getNode i = asNode <=< (V.!? i) . graphNodes

-- | Convert matrix that defines value of node and edges
fromMatrix :: Vector n -> Matrix (Maybe e) -> Graph (EdgeNode e n)
fromMatrix verts m = Graph (V.map JustNode verts) (V.map (fmap JustEdge) edges)
  where
  n = V.length verts
  edges = V.fromList $ foldMatrix f [] m
  f !row !col !acc !mv = case mv of
    Just v | row < n && col < n -> GraphEdge row col v : acc
    _ -> acc

-- | Convert graph to edge tags matrix. If graph has edge from i to j with tag 'e',
-- the resulted matrix will contain [i][j] = Just e. For [i][i] = 0
edgeMatrix :: Num e => Graph (EdgeNode e n) -> Matrix (Maybe e)
edgeMatrix g = makeMatrix n n $ \i j -> if i == j then Just 0 else getEdge i j g
  where n = graphVerts g

-- | Convert graph to node to node matrix. If graph has edge from i(tag ni) to j(tag nj) with tag 'e',
-- the resulted matrix will contain [i][j] = Just j. For [i][i] = Just i
nodeMatrix :: Graph (EdgeNode e n) -> Matrix (Maybe NodeId)
nodeMatrix g = makeMatrix n n $ \i j -> if | i == j -> Just i
                                           | hasEdge i j g -> Just j
                                           | otherwise -> Nothing
  where n = graphVerts g

-- | Tag nodes with their ids.
indexNodes :: Graph (EdgeNode e n) -> Graph (EdgeNode e (NodeId, n))
indexNodes = imapNodes ((,))

data EdgeNode e n = JustEdge e | JustNode n
  deriving (Show)

instance Bifunctor EdgeNode where
  bimap f1 f2 e = case e of
    JustEdge e -> JustEdge (f1 e)
    JustNode n -> JustNode (f2 n)
  {-# INLINEABLE bimap #-}

newtype GraphEdgeNode a b = GraphEdgeNode { unGraphEdgeNode :: Graph (EdgeNode a b) }

instance Bifunctor GraphEdgeNode where
  bimap f1 f2 (GraphEdgeNode g) = GraphEdgeNode g {
      graphNodes = V.map (bimap f1 f2) $ graphNodes g
    , graphEdges = V.map (fmap (bimap f1 f2)) $ graphEdges g
    }
  {-# INLINEABLE bimap #-}

asEdge :: EdgeNode e n -> Maybe e
asEdge (JustEdge e) = Just e
asEdge JustNode{} = Nothing

asNode :: EdgeNode e n -> Maybe n
asNode (JustNode n) = Just n
asNode JustEdge{} = Nothing

getGraphNodes :: Graph (EdgeNode e n) -> [n]
getGraphNodes g = catMaybes . fmap asNode . V.toList $ graphNodes g

getGraphEdges :: Graph (EdgeNode e n) -> [GraphEdge e]
getGraphEdges g = catMaybes . V.toList . V.map sequence . V.filter (isJust . edgeData) . V.map (fmap asEdge) $ graphEdges g

mapNodes :: (a -> b) -> Graph (EdgeNode e a) -> Graph (EdgeNode e b)
mapNodes f g = g {
    graphNodes = V.map f' $ graphNodes g
  , graphEdges = V.map (fmap f') $ graphEdges g
  }
  where
    f' (JustNode n) = JustNode (f n)
    f' (JustEdge e) = JustEdge e

imapNodes :: (NodeId -> a -> b) -> Graph (EdgeNode e a) -> Graph (EdgeNode e b)
imapNodes f g = g {
    graphNodes = V.imap f' $ graphNodes g
  , graphEdges = V.imap (\i -> fmap (f' i)) $ graphEdges g
  }
  where
    f' i (JustNode n) = JustNode (f i n)
    f' _ (JustEdge e) = JustEdge e

type LayerNum = Int

-- | Group graph by layers. Wher first layer is nodes that doesn't have any incoming edges.
-- Second layer contains nodes that have inputs from only first layer. Third layer contains nodes
-- that have inputs from only first and second layer. And so on.
layerGraph :: forall e n . Graph (EdgeNode e n) -> Maybe (Graph (EdgeNode e (LayerNum, n)))
layerGraph = go 0 mempty . mapNodes (-1,)
  where
    go :: LayerNum -> IS.IntSet -> Graph (EdgeNode e (LayerNum, n)) -> Maybe (Graph (EdgeNode e (LayerNum, n)))
    go !i !pnodes !gr
      | IS.null nnodes = if i == 0 then Nothing else Just gr
      | otherwise = go (i+1) (pnodes `IS.union` nnodes) newgr
      where
        goodEdge n (GraphEdge from to _) = to /= n || IS.member from pnodes
        nodeInClass n = not (IS.member n pnodes) && all (goodEdge n) (getGraphEdges gr)
        nnodes = IS.fromList $ filter nodeInClass [0 .. graphVerts gr - 1]
        newgr = imapNodes (\ni (oldi, n) -> if IS.member ni nnodes then (i, n) else (oldi, n)) gr

unlayerGraph :: forall e n . Graph (EdgeNode e (LayerNum, n)) -> Graph (EdgeNode e n)
unlayerGraph = mapNodes snd

data PathStep e n = PathStep {
    pathStepNodeId :: !NodeId
  , pathStepNode   :: !n
  , pathStepEdge   :: !e
  } deriving (Show)

type Path e n = NonEmpty (PathStep e n)

floydWarshall :: forall e n . (Num e, Ord e, Bounded e, Show e) => NodeId -> NodeId -> Graph (EdgeNode e n) -> (Maybe (Path e n), Matrix (Maybe e), Matrix (Maybe NodeId))
floydWarshall starti endi gr = (mkPath dist next, dist, next)
  where
    n = graphVerts gr
    dist0 = edgeMatrix gr :: Matrix (Maybe e)
    next0 = nodeMatrix gr :: Matrix (Maybe NodeId)

    (dist, next) = foldl' (\acck k ->
        foldl' (\accj j ->
          foldl' (\acci i -> go acci i j k) accj [0 .. n-1]
        ) acck [0 .. n-1]
      ) (dist0, next0) [0 .. n-1]
      where
        go :: (Matrix (Maybe e), Matrix (Maybe NodeId)) -> Int -> Int -> Int -> (Matrix (Maybe e), Matrix (Maybe NodeId))
        go (!dist, !next) i j k
          | Just ik <- atd i k, Just kj <- atd k j, Nothing <- atd i j = newMatricies $ ik + kj
          | Just ik <- atd i k, Just kj <- atd k j, Just ij <- atd i j, ij > ik + kj = newMatricies $ ik + kj
          | otherwise = (dist, next)
          where
            newMatricies v = ( matrixSet i j (Just v) dist, matrixSet i j (at next i k) next)

            atd :: Int -> Int -> Maybe e
            atd i j = at dist i j

    at :: forall a . Matrix a -> Int -> Int -> a
    at = matrixAtUnsafe

    mkPath :: Matrix (Maybe e) -> Matrix (Maybe NodeId) -> Maybe (Path e n)
    mkPath dist next = case at next starti endi of
      Nothing -> Nothing
      Just u -> do
        sn <- getNode starti gr
        pure $ NE.reverse $ go (PathStep starti sn 0 :| []) u
      where
        go :: Path e n -> NodeId -> Path e n
        go !acc u | u == endi = fromMaybe acc $ do
                      un <- getNode u gr
                      pure $ PathStep u un 0 NE.<| acc
                  | otherwise = fromMaybe acc $ do
                      v <- at next u endi
                      e <- at dist u endi
                      un <- getNode u gr
                      let acc' = PathStep u un e NE.<| acc
                      pure $ go acc' v
