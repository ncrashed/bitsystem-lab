self: super:
let haskellOld = super.haskell;
    pkgs = self;
    lib = pkgs.lib;
in rec {
  haskell = haskellOld // {
    packages = haskellOld.packages // {
      ghc865 = haskellOld.packages.ghc865.override (old: {
        overrides = lib.composeExtensions (old.overrides or (_: _: {})) (self: super: {
          time-compat = pkgs.haskell.lib.dontCheck super.time-compat;
        });
      });
    };
  };
}
