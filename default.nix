{ minimize ? false }:
let
  reflex-platform = import ./reflex-platform.nix {
    nixpkgsOverlays = [ (import ./fix-wsl.nix) ];
  };
in reflex-platform.project ({ pkgs, ... }: {
  packages = {
    lab-front = ./front;
    lab-shared = ./shared;
    lab-back = ./back;
    pixi = ./pixi;
  };
  shells = {
    ghcjs = ["lab-front" "lab-shared" "pixi"];
    ghc = ["lab-back" "lab-front" "lab-shared" "pixi"]; # We can build front by GHC to enable use of IDE tools for it
  };
  overrides = import ./overrides.nix { inherit reflex-platform minimize; };
})
