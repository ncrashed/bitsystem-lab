{ mkDerivation, array, base, containers, criterion, exceptions
, fetchgit, linear, mtl, QuickCheck, stdenv, template-haskell
, vector
}:
mkDerivation {
  pname = "apecs";
  version = "0.9.2";
  src = fetchgit {
    url = "https://github.com/jonascarpay/apecs.git";
    sha256 = "13wh54vwk16w79xb6n1j8q14sh1nfdfy73ijla8bn4nhhhq9gnvq";
    rev = "45e76a8e46b167be0aa4a0c75385cc036d62dff5";
    fetchSubmodules = true;
  };
  postUnpack = "sourceRoot+=/apecs; echo source root reset to $sourceRoot";
  libraryHaskellDepends = [
    array base containers exceptions mtl template-haskell vector
  ];
  testHaskellDepends = [ base containers linear QuickCheck vector ];
  benchmarkHaskellDepends = [ base criterion linear ];
  homepage = "https://github.com/jonascarpay/apecs#readme";
  description = "Fast Entity-Component-System library for game programming";
  license = stdenv.lib.licenses.bsd3;
}
