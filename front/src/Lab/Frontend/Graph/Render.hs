module Lab.Frontend.Graph.Render(
    drawNodes
  , drawEdges
  ) where

import Apecs
import Apecs.Core
import Control.Monad
import Control.Monad.IO.Class
import Data.Graphics.Pixi.App
import Data.Graphics.Pixi.Graphics
import Data.Graphics.Pixi.Text
import Data.Text (Text, pack)
import Data.Typeable
import Data.Default
import Lab.Frontend.Graph.System
import Lab.Frontend.Graph.Types

import qualified Data.Vector.Unboxed as V

drawNodes :: forall n e . (Typeable n, Show n) => GraphSys n e ()
drawNodes = do
  s :: Storage (Dirty, Node n, Pos, Entity) <- getStore
  es <- explMembers s
  V.forM_ es $ \e -> do
    (Dirty v, n, p, e) <- explGet s e
    when v $ do
      isSel <- isSelected e
      liftJSM $ drawNode isSel n p
      set e $ Dirty False

drawNode :: (MonadJSM m, Show n) => Bool -> Node n -> Pos -> m ()
drawNode isSel n@Node{..} (Pos v@(V2 x y)) = do
  graphicsClear nodeGraphics
  lineStyle nodeGraphics 2 borderColor 1
  let c = if isSel then selNodeColor else nodeColor
  beginFill nodeGraphics c 1
  drawCircle nodeGraphics x y nodeRadius
  endFill nodeGraphics
  drawNodeText nodeLabel v (pack . show $ nodeState)

drawEdges :: forall n e . (Show e, Typeable e, Typeable n) => GraphSys n e ()
drawEdges = do
  s :: Storage (Edge e) <- getStore
  es <- explMembers s
  V.forM_ es $ \e -> do
    edge@(Edge e1 e2 _ _ g _) <- explGet s e
    (Dirty d1, n1, p1) <- get e1
    (Dirty d2, n2, p2) <- get e2
    when (d1 || d2) $ do
      drawEdge edge n1 p1 n2 p2

drawEdge :: Show e => Edge e -> Node n -> Pos -> Node n -> Pos -> GraphSys n e ()
drawEdge e n1 (Pos p1) n2 (Pos p2) = do
  let g = edgeGraphics e
  graphicsClear g
  lineStyle g 2.5 borderColor 1
  let v1@(V2 px1 py1) = p1 + vecScale (nodeRadius n1) (vecNorm $ p2 - p1)
      v2@(V2 px2 py2) = p2 + vecScale (nodeRadius n2) (vecNorm $ p1 - p2)
  moveTo g px1 py1
  lineTo g px2 py2
  let arrowLen = 8
  drawArrowHead g v2 (vecNorm $ v2 - v1) (pi / 6) arrowLen
  drawEdgeText (edgeLabel e) (v1 + aboveVector (v2 - v1) 20) (pack . show $ edgeState e)

-- | Helper to draw arrow tip
drawArrowHead :: MonadJSM m
  => PixiGraphics
  -> V2 Float -- ^ Position of arrow tip ending
  -> V2 Float -- ^ Direction the arrow facing
  -> Float -- ^ Radians of angle between the diration and arrow wings
  -> Float -- ^ Lengh of arrow wings
  -> m ()
drawArrowHead g p@(V2 x y) dv av l = do
  drawWing av
  drawWing (negate av)
  where
    drawWing a = do
      moveTo g x y
      let V2 x1 y1 = p + vecRotate a (vecScale l $ negate dv)
      lineTo g x1 y1

-- | Helper to draw edge text
drawEdgeText :: MonadJSM m
  => PixiText
  -> V2 Float -- ^ Position of text
  -> Text
  -> m ()
drawEdgeText pt (V2 x y) str = do
  textSetX pt x
  textSetY pt y
  textSetText pt str

-- | Helper to draw edge text
drawNodeText :: MonadJSM m
  => PixiText
  -> V2 Float -- ^ Position of text
  -> Text
  -> m ()
drawNodeText pt (V2 x y) str = do
  textSetX pt x
  textSetY pt y
  textSetText pt str
