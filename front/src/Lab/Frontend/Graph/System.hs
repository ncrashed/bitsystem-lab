module Lab.Frontend.Graph.System(
    getDimensions
  , setDimensions
  , getMouse
  , setMouse
  , getSelected
  , getSelected2
  , setSelected
  , isSelected
  , applyVels
  , getNodeEdges
  , applyBorderForces
  , applyEdgesForce
  , applyNodesForce
  , applyNodesEdgesForce
  , applySelectedForce
  , setNodeDirty
  , makeEdge
  , MouseEvent(..)
  , onMousePick
  , onMouseSelect
  , genNode
  , genNodePosition
  , centrifyAll
  ) where

import Apecs
import Apecs.Core
import Control.Monad
import Control.Monad.IO.Class
import Data.Default
import Data.Foldable (traverse_, for_)
import Data.Graphics.Pixi.App
import Data.Graphics.Pixi.Graphics
import Data.Graphics.Pixi.Text
import Data.List (nub)
import Data.Maybe
import Data.Text (pack)
import Data.Traversable (for)
import Data.Typeable
import Data.Vector (Vector)
import Lab.Frontend.Graph.Types
import Safe
import System.Random

import Lab.Graph

import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as VU

import Debug.Trace

getDimensions :: GraphSys n e (V2 Int)
getDimensions = do
  s :: Storage Dimensions <- getStore
  Dimensions d <- explGet s (unEntity global)
  pure d

setDimensions :: Int -> Int -> GraphSys n e ()
setDimensions w h = do
  s :: Storage Dimensions <- getStore
  explSet s (unEntity global) (Dimensions $ V2 w h)

getMouse :: GraphSys n e (V2 Float)
getMouse = do
  s :: Storage Mouse <- getStore
  Mouse d <- explGet s (unEntity global)
  pure d

setMouse :: Float -> Float -> GraphSys n e ()
setMouse x y = do
  s :: Storage Mouse <- getStore
  explSet s (unEntity global) (Mouse $ V2 x y)

getSelected :: GraphSys n e [Entity]
getSelected = do
  s :: Storage Selected <- getStore
  Selected d <- explGet s (unEntity global)
  pure d

getSelected2 :: GraphSys n e (Maybe (Entity, Entity))
getSelected2 = do
  es <- getSelected
  pure $ case es of
    e1 : e2 : _ -> Just (e1, e2)
    _ -> Nothing

setSelected :: [Entity] -> GraphSys n e ()
setSelected me = do
  s :: Storage Selected <- getStore
  explSet s (unEntity global) (Selected me)

addSelected :: [Entity] -> GraphSys n e ()
addSelected me = do
  ss <- getSelected
  setSelected $ nub $ ss ++ me

isSelected :: Entity -> GraphSys n e Bool
isSelected e = do
  es <- getSelected
  pure $ e `elem` es

centrifyAll :: Typeable n => GraphSys n e ()
centrifyAll = do
  V2 w h <- getDimensions
  let viewCenter = V2 (fromIntegral w * 0.5) (fromIntegral h * 0.5)
  s :: Storage (Pos, Mass) <- getStore
  es <- explMembers s
  (mr, ms) <- VU.foldM (collectMass s) (0, 0) es
  let cv = vecScale (recip ms) mr
  VU.forM_ es $ \e -> do
    (Pos p, _) <- explGet s e
    set (Entity e) $ Pos $ viewCenter + (p - cv)
  where
    collectMass :: Storage (Pos, Mass) -> (V2 Float, Float) -> Int -> GraphSys n e (V2 Float, Float)
    collectMass s (!accr, !accm) ei = do
      (Pos p, Mass m) <- explGet s ei
      pure (accr + vecScale m p, accm + m)

applyVels :: Typeable n => GraphSys n e ()
applyVels = do
  s :: Storage (Pos, Vel, Mass) <- getStore
  es <- explMembers s
  VU.forM_ es $ \e -> do
    applyBorderForces (Entity e)
    -- applyCenterForces (Entity e)
    (p, Vel v, Mass m) <- explGet s e
    unless (isZeroVel $ Vel v) $ do
      let v' = v - vecScale envFriction v
      set (Entity e) $ (Vel v', applyVel p $ Vel v', Dirty True)

applyBorderForces :: forall n e . Typeable n => Entity -> GraphSys n e ()
applyBorderForces e = do
  V2 canvasWidth canvasHeight <- getDimensions
  (Pos (V2 px py), Vel v, Mass m, Node{..} :: Node n) <- get e
  let cw = fromIntegral canvasWidth - nodeRadius
      ch = fromIntegral canvasHeight - nodeRadius
  let v1 = if px < nodeRadius  then v  + vecScale (100 * edgeDefK * (nodeRadius - px) * recip m) (V2 1 0) else v
  let v2 = if px > cw          then v1 + vecScale (100 * edgeDefK * (cw - px)         * recip m) (V2 1 0) else v1
  let v3 = if py < nodeRadius  then v2 + vecScale (100 * edgeDefK * (nodeRadius - py) * recip m) (V2 0 1) else v2
  let v4 = if py > ch          then v3 + vecScale (100 * edgeDefK * (ch - py)         * recip m) (V2 0 1) else v3
  set e $ Vel v4

applyCenterForces :: forall n e . Typeable n => Entity -> GraphSys n e ()
applyCenterForces e = do
  V2 canvasWidth canvasHeight <- getDimensions
  (Pos p@(V2 px py), Vel v, Mass m, Node{..} :: Node n) <- get e
  let cw = fromIntegral canvasWidth * 0.5
      ch = fromIntegral canvasHeight * 0.5
      dv = p - V2 cw ch
      d  = vecDist 0 dv
      n  = vecNorm dv
      v1 = v - vecScale (min maxAttraction $ centerAttraction * d * d) n
  set e $ Vel v1

setNodeDirty :: Entity -> Bool -> GraphSys n e ()
setNodeDirty e v = modify e $ \(Dirty _) -> Dirty v

genNodePosition :: MonadIO m => V2 Int -> m (V2 Float)
genNodePosition (V2 canvasWidth canvasHeight) = liftIO $  do
  x <- randomRIO (defNodeRadius, fromIntegral canvasWidth - defNodeRadius)
  y <- randomRIO (defNodeRadius, fromIntegral canvasHeight - defNodeRadius)
  pure $ V2 x y

data MouseEvent = PointerDown !Entity | PointerUp !Entity | PointerUpOutside !Entity

onMousePick :: forall n e . (Typeable n, Show n) => MouseEvent -> GraphSys n e ()
onMousePick ev = case ev of
  PointerDown e -> setSelected [e]
  PointerUp _ -> setSelected []
  PointerUpOutside _ -> setSelected []

onMouseSelect :: forall n e . (Typeable n, Show n) => MouseEvent -> GraphSys n e ()
onMouseSelect ev = case ev of
  PointerDown e -> addSelected [e]
  PointerUp _ -> pure ()
  PointerUpOutside _ -> setSelected []

genNode :: forall n e . (Typeable n, Show n)
  => (MouseEvent -> GraphSys n e ()) -- ^ Action on mouse events
  -> PixiApp -> Float -> n -> V2 Float -> GraphSys n e (Entity, Pos)
genNode onMouse app size a (V2 x y) = do
  V2 canvasWidth canvasHeight <- getDimensions
  g <- newGraphics
  graphicsSetInteractive g True
  pixiAddChild app g
  pt <- createLabel
  let p = Pos $ V2 x y
      node = Node {
          nodeRadius = size
        , nodeColor = defNodeColor
        , nodeGraphics = g
        , nodeLabel = pt
        , nodeState = a
        }
  e <- newEntity (node, p, Vel 0, defNodeMass, Dirty True)
  w <- ask
  graphicsSetOnPointerDown g $ runWith w $ onMouse $ PointerDown e
  graphicsSetOnPointerUp g $ runWith w $ onMouse $ PointerUp e
  graphicsSetOnPointerUpOutside g $ runWith w $ onMouse $ PointerUpOutside e
  pure (e, p)
  where
    createLabel = do
      let tstr = pack . show $ a
          tstyle =  def {
              ptextFontSize = 18
            , ptextFontStyle = "bold"
            , ptextFill = textColor
            }
      pt <- newPixiText tstr tstyle
      textSetAnchor pt 0.5 0.5
      pixiAddChild app pt
      pure pt

getNodeEdges :: forall n e . Typeable e => Entity -> GraphSys n e [Entity]
getNodeEdges nodeE = do
  s :: Storage (Edge e, Entity) <- getStore
  es <- explMembers s
  fmap catMaybes $ for (VU.toList es) $ \e -> do
    (edge, ee) <- explGet s e
    pure $ if edgeStart edge == nodeE || edgeEnd edge == nodeE then Just ee else Nothing

hasEdgesBetween :: forall n e . Typeable e => Entity -> Entity -> GraphSys n e Bool
hasEdgesBetween e1 e2 = do
  s :: Storage (Edge e) <- getStore
  es <- explMembers s
  fmap (isJust . headMay . catMaybes) $ for (VU.toList es) $ \e -> do
    edge <- explGet s e
    let case1 = edgeStart edge == e1 && edgeEnd edge == e2
        case2 = edgeStart edge == e2 && edgeEnd edge == e1
    pure $ if case1 || case2 then Just () else Nothing

edgesCount :: Int
edgesCount = 6


makeEdge :: forall n e . (Typeable e, Show e, Integral e) => PixiApp -> Vector (Entity, Pos) -> GraphEdge e -> GraphSys n e ()
makeEdge app xs (GraphEdge i1 i2 est) = do
  let e1 = fst $ xs V.! i1
      e2 = fst $ xs V.! i2
  hasN <- hasEdgesBetween e1 e2
  unless hasN $ do
    g <- newGraphics
    pixiAddChild app g
    pt <- createLabel
    let l = edgeDefLength * fromIntegral est
    _ <- newEntity (Edge e1 e2 l est g pt)
    pure ()
  where
    createLabel = do
      let tstr = pack . show $ est
          tstyle =  def {
              ptextFontSize = 24
            , ptextFontStyle = "bold"
            , ptextFill = textColor
            }
      pt <- newPixiText tstr tstyle
      textSetAnchor pt 0.5 0.5
      pixiAddChild app pt
      pure pt

applyEdgesForce :: forall n e . Typeable e => GraphSys n e ()
applyEdgesForce = do
  s :: Storage (Edge e) <- getStore
  es <- explMembers s
  VU.forM_ es $ \e -> do
    Edge e1 e2 l _ g _ <- explGet s e
    (Pos p1, Mass m1, Vel v1) <- get e1
    (Pos p2, Mass m2, Vel v2) <- get e2
    let d = vecDist 0 (p2 - p1)
        fv = (d - l) * edgeDefK
        f1 = vecScale (recip m1 * fv) $ vecNorm (p2 - p1)
        f2 = vecScale (recip m2 * fv) $ vecNorm (p1 - p2)
    set e1 $ Vel $ v1 + f1
    set e2 $ Vel $ v2 + f2

applyNodesForce :: forall n e . Typeable n => GraphSys n e ()
applyNodesForce = do
  s :: Storage (Node n) <- getStore
  es <- explMembers s
  flip VU.imapM_ es $ \i i1 -> do
    let e1 = Entity i1
    (Pos p1, Mass m1, Vel v1) <- get e1
    VU.forM_ (VU.drop (i+1) es) $ \i2 -> do
      let e2 = Entity i2
      (Pos p2, Mass m2, Vel v2) <- get e2
      let d = vecDist 0 (p2 - p1)
          fv = negate $ nodeRepForce * recip (d * d)
          f1 = vecScale (recip m1 * fv) $ vecNorm (p2 - p1)
          f2 = vecScale (recip m2 * fv) $ vecNorm (p1 - p2)
      set e1 $ Vel $ v1 + f1
      set e2 $ Vel $ v2 + f2

applyNodesEdgesForce :: forall n e . (Typeable n, Typeable e) => GraphSys n e ()
applyNodesEdgesForce = do
  snode :: Storage (Node n) <- getStore
  sedge :: Storage (Edge e) <- getStore
  ns <- explMembers snode
  es <- explMembers sedge
  VU.forM_ ns $ \i1 -> do
    let e1 = Entity i1
    (Pos p1, Mass m1, Vel v1) <- get e1
    VU.forM_ es $ \i2 -> do
      let e2 = Entity i2
      Edge e21 e22 _ _ _ _ <- explGet sedge i2
      when (e21 /= e1 && e22 /= e1) $ do
        Pos p21 <- get e21
        Pos p22 <- get e22
        let p2 = vecScale 0.5 $ p22 + p21
            d = vecDist 0 (p2 - p1)
            fv = negate $ (10 * nodeRepForce) * recip (d * d)
            f1 = vecScale (recip m1 * fv) $ vecNorm (p2 - p1)
            -- f2 = vecScale (recip m2 * fv) $ vecNorm (p1 - p2)
        set e1 $ Vel $ v1 + f1
        -- set e2 $ Vel $ v2 + f2

applySelectedForce :: forall n e . Typeable n => GraphSys n e ()
applySelectedForce = do
  s :: Storage (Node n) <- getStore
  es <- explMembers s
  VU.forM_ es $ \i -> do
    let e = Entity i
    isSel <- isSelected e
    when isSel $ do
      (Pos p1, Mass m1, Vel v1) <- get e
      p2 <- getMouse
      let d = vecDist 0 (p2 - p1)
          fv = d * edgeSelK
          f = vecScale (recip m1 * fv) $ vecNorm (p2 - p1)
      set e $ Vel $ v1 + f
