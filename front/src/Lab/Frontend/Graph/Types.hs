module Lab.Frontend.Graph.Types(
    MonadJSM
  , liftJSM
  , textColor
  , borderColor
  , defNodeColor
  , selNodeColor
  , edgeDefLength
  , edgeDefK
  , edgeSelK
  , nodeRepForce
  , defNodeRadius
  , defNodeMass
  , envFriction
  , centerAttraction
  , maxAttraction
  , Pos(..)
  , Vel(..)
  , isZeroVel
  , applyVel
  , Mass(..)
  , Dirty(..)
  , Dimensions(..)
  , Selected(..)
  , Mouse(..)
  , Node(..)
  , getClosestNodes
  , Edge(..)
  , edgeOtherSide
  , World(..)
  , GraphSys
  , newWorld
  , V2(..)
  , vecDist2
  , vecDist
  , vecScale
  , vecNorm
  , vecRotate
  , aboveVector
  ) where

import Apecs
import Apecs.Core
import Control.Monad.IO.Class
import Data.Coerce
import Data.Graphics.Pixi.Graphics
import Data.Graphics.Pixi.Text
import Data.List (sortBy)
import Data.Ord
import Language.Javascript.JSaddle
import Linear

import Debug.Trace

textColor :: Color
textColor = 0x563f03

borderColor :: Color
borderColor = 0x915e07

defNodeColor :: Color
defNodeColor = 0xffc61c

selNodeColor :: Color
selNodeColor = 0x0ffc61

edgeDefLength :: Float
edgeDefLength = 50

edgeDefK :: Float
edgeDefK = 0.8

edgeSelK :: Float
edgeSelK = 1.5

nodeRepForce :: Float
nodeRepForce = 500000

defNodeRadius :: Radius
defNodeRadius = 15

defNodeMass :: Mass
defNodeMass = Mass 10

envFriction :: Float
envFriction = 0.8

centerAttraction :: Float
centerAttraction = 0.00001

maxAttraction :: Float
maxAttraction = 10

vecLength :: Floating a => V2 a -> a
vecLength v = sqrt $ vecScalar v v
{-# INLINABLE vecLength #-}

vecDist2 :: Num a => V2 a -> V2 a -> a
vecDist2 (V2 x1 y1) (V2 x2 y2) = (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)
{-# INLINABLE vecDist2 #-}

vecDist :: Floating a => V2 a -> V2 a -> a
vecDist p1 p2 = sqrt $ vecDist2 p1 p2
{-# INLINABLE vecDist #-}

vecScale :: Num a => a -> V2 a -> V2 a
vecScale v (V2 x y) = V2 (x*v) (y*v)
{-# INLINABLE vecScale #-}

vecNorm :: (Num a, Floating a) => V2 a -> V2 a
vecNorm p = vecScale (recip $ vecDist 0 p) p
{-# INLINABLE vecNorm #-}

vecRotate :: Floating a => a -> V2 a -> V2 a
vecRotate a (V2 x y) = V2 x' y'
  where
    cs = cos a
    sn = sin a
    x' = x * cs - y * sn
    y' = x * sn + y * cs
{-# INLINABLE vecRotate #-}

vecScalar :: Floating a => V2 a -> V2 a -> a
vecScalar (V2 x1 y1) (V2 x2 y2) = x1*x2 + y1*y2
{-# INLINABLE vecScalar #-}

vecAngle :: Floating a => V2 a -> V2 a -> a
vecAngle v1 v2 = acos $ vecScalar v1 v2 / (vecLength v1 * vecLength v2 )
{-# INLINABLE vecAngle #-}

aboveVector :: (Floating a, Ord a) => V2 a -> a -> V2 a
aboveVector v d = vecScale (factor * d) n + vecScale 0.5 v
  where
    n = vecNorm $ vecRotate (pi / 2) v
    factor = if vecAngle v (V2 1 0) < pi / 2 then (-1) else 1
{-# INLINABLE aboveVector #-}

newtype Pos = Pos (V2 Float)
  deriving (Num)

instance Component Pos where
  type Storage Pos = Map Pos

newtype Vel = Vel (V2 Float)
  deriving (Num)

instance Component Vel where
  type Storage Vel = Map Vel

isZeroVel :: Vel -> Bool
isZeroVel (Vel (V2 x y)) = abs (x + y) < 0.001

applyVel :: Pos -> Vel -> Pos
applyVel (Pos pv) (Vel vv) = Pos $ pv + vv

newtype Mass = Mass Float

instance Component Mass where
  type Storage Mass = Map Mass

newtype Dirty = Dirty Bool

instance Component Dirty where
  type Storage Dirty = Map Dirty

newtype Dimensions = Dimensions (V2 Int)

instance Semigroup Dimensions where
  _ <> a = a

instance Monoid Dimensions where
  mempty = Dimensions (V2 0 0)

instance Component Dimensions where
  type Storage Dimensions = Global Dimensions

newtype Selected = Selected [Entity]

instance Semigroup Selected where
  _ <> a = a

instance Monoid Selected where
  mempty = Selected []

instance Component Selected where
  type Storage Selected = Global Selected

newtype Mouse = Mouse (V2 Float)

instance Semigroup Mouse where
  _ <> a = a

instance Monoid Mouse where
  mempty = Mouse 0

instance Component Mouse where
  type Storage Mouse = Global Mouse

data Node a = Node {
  nodeRadius   :: !Radius
, nodeColor    :: !Color
, nodeGraphics :: !PixiGraphics
, nodeLabel    :: !PixiText
, nodeState    :: !a
}

instance Component (Node a) where
  type Storage (Node a) = Map (Node a)

getClosestNodes :: Pos -> [(Entity, Pos)] -> [Entity]
getClosestNodes (Pos p) = fmap fst . sortBy (comparing $ vecDist2 p . coerce . snd)

data Edge a = Edge {
  edgeStart     :: !Entity
, edgeEnd       :: !Entity
, edgeLength    :: !Float
, edgeState     :: !a
, edgeGraphics  :: !PixiGraphics
, edgeLabel     :: !PixiText
}

instance Component (Edge a) where
  type Storage (Edge a) = Map (Edge a)

edgeOtherSide :: Edge e -> Entity -> Entity
edgeOtherSide Edge{..} e
  | edgeStart == e = edgeEnd
  | otherwise = edgeStart

data World n e = World {
  worldEntities   :: !(Storage EntityCounter)
, worldPoses      :: !(Storage Pos)
, worldVels       :: !(Storage Vel)
, worldMasses     :: !(Storage Mass)
, worldDirties    :: !(Storage Dirty)
, worldNodes      :: !(Storage (Node n))
, worldEdges      :: !(Storage (Edge e))
, worldDimensions :: !(Storage Dimensions)
, worldSelected   :: !(Storage Selected)
, worldMouse      :: !(Storage Mouse)
}

type GraphSys n e = SystemT (World n e) JSM

instance Monad m => Has (World n e) m EntityCounter where
  getStore = asks worldEntities
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m Pos where
  getStore = asks worldPoses
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m Vel where
  getStore = asks worldVels
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m Mass where
  getStore = asks worldMasses
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m Dirty where
  getStore = asks worldDirties
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m (Node n) where
  getStore = asks worldNodes
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m (Edge e) where
  getStore = asks worldEdges
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m Dimensions where
  getStore = asks worldDimensions
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m Selected where
  getStore = asks worldSelected
  {-# INLINE getStore #-}

instance Monad m => Has (World n e) m Mouse where
  getStore = asks worldMouse
  {-# INLINE getStore #-}

newWorld :: MonadIO m => m (World n e)
newWorld = liftIO $ World
  <$> explInit
  <*> explInit
  <*> explInit
  <*> explInit
  <*> explInit
  <*> explInit
  <*> explInit
  <*> explInit
  <*> explInit
  <*> explInit

#ifndef ghcjs_HOST_OS

instance MonadJSM m => MonadJSM (SystemT w m) where
  liftJSM' = lift . liftJSM'
  {-# INLINE liftJSM' #-}

#endif
