module Lab.Frontend.Monad(
    Front
  , MonadIO(..)
  , Text
  , unpack
  , pack
  , runFront
  , join
  , module Reflex.Dom
  , module Reflex.Dom.Retractable
  , module Lab.Frontend.Id
  , module Lab.Frontend.Utils
  , module Data.Default
  , void
  ) where

import Control.Monad (join)
import Control.Monad.Fix
import Control.Monad.IO.Class
import Data.Default
import Data.Functor (void)
import Data.Text (Text, unpack, pack)
import JSDOM.Types (MonadJSM)
import Lab.Frontend.Id
import Lab.Frontend.Utils
import Reflex.Dom
import Reflex.Dom.Retractable

type FrontConstr t m = (MonadHold t m
  , PostBuild t m
  , DomBuilder t m
  , MonadFix m
  , TriggerEvent t m
  , PerformEvent t m
  , MonadIO (Performable m)
  , DomBuilderSpace m ~ GhcjsDomSpace
  , MonadIO m
  , MonadJSM m
  , MonadJSM (Performable m)
  )

type Front t m a = FrontConstr t m => RetractT t m a

runFront :: FrontConstr t m => Front t m a -> m a
runFront = runRetract
