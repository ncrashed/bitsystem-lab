module Lab.Frontend.Id(
    genId
  ) where

import Control.Monad.IO.Class
import Data.IORef
import Data.Text (Text, pack)
import System.IO.Unsafe (unsafePerformIO)

-- | Generate new unique id
genId :: MonadIO m => m Text
genId = do
  i <- liftIO $ atomicModifyIORef' idRef $ \i -> (i+1, i)
  pure $ pack . show $ i

idRef :: IORef Int
idRef = unsafePerformIO $ newIORef 0
{-# NOINLINE idRef #-}
