{-# LANGUAGE OverloadedLists #-}
module Lab.Frontend.Utils(
    sampleDyn
  , widgetHoldDyn
  , widgetHoldEvent
  , performDynamic
  , showt
  , splitMay
  , container
  , form
  , fieldset
  , label
  , spanClass
  , labelFor
  , errorDiv
  ) where

import Reflex
import Reflex.Dom
import Data.Text (Text, pack)

sampleDyn :: (MonadSample t m, Reflex t) => Dynamic t a -> m a
sampleDyn = sample . current

widgetHoldDyn :: (MonadHold t m, Adjustable t m) => Dynamic t (m a) -> m (Dynamic t a)
widgetHoldDyn dma = do
   ma <- sampleDyn dma
   widgetHold ma $ updated dma

widgetHoldEvent :: (MonadHold t m, Adjustable t m) => m (Event t a) -> Event t (m (Event t a)) -> m (Event t a)
widgetHoldEvent m0 em = switchDyn <$> widgetHold m0 em

performDynamic :: (MonadHold t m, PerformEvent t m, PostBuild t m)
  => a -> Dynamic t (Performable m a) -> m (Dynamic t a)
performDynamic a0 dma = do
  buildE <- getPostBuild
  ma <- sampleDyn dma
  e0 <- performEvent $ ma <$ buildE
  e <- performEvent $ updated dma
  holdDyn a0 $ leftmost [e, e0]

showt :: Show a => a -> Text
showt = pack . show

splitMay :: Reflex t => Event t (Maybe a) -> (Event t (Maybe ()), Event t a)
splitMay e = (errE, resE)
  where
    errE = maybe (Just ()) (const Nothing) <$> e
    resE = fmapMaybe id e

container :: DomBuilder t m => m a -> m a
container = divClass "container"

form :: DomBuilder t m => m a -> m a
form = el "form"

fieldset :: DomBuilder t m => m a -> m a
fieldset = el "fieldset"

label :: DomBuilder t m => m a -> m a
label = el "label"

labelFor :: DomBuilder t m => Text -> m a -> m a
labelFor i = elAttr "label" [("for", i)]

errorDiv :: (DomBuilder t m, MonadHold t m) => Event t (Maybe Text) -> m ()
errorDiv e = widgetHold_ (pure ()) $ ffor e $ maybe (pure ()) (divClass "error" . text)

spanClass :: DomBuilder t m => Text -> m a -> m a
spanClass = elClass "span"
