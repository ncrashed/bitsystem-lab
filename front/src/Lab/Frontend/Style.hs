module Lab.Frontend.Style(
    frontendCss
  , frontendCssBS
  ) where

import Clay
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Text.Lazy.Encoding (encodeUtf8)
import Lab.Frontend.Style.TH
import Prelude hiding ((**))

frontendCssBS :: ByteString
frontendCssBS = let
  selfcss = toStrict . encodeUtf8 . renderWith compact [] $ frontendCss
  in milligramCss <> selfcss

colWidth = px 30

frontendCss :: Css
frontendCss = do
  html ? textAlign center
  form ? textAlign (alignSide sideLeft)
  "button" ? do
    marginRight $ px 10
  ".container" ? do
    maxWidth $ px 5000
  ".error" ? do
    fontSize $ pt 14
    fontColor $ rgb 220 53 69
  ".matrix-row" ** "input" ? do
    maxWidth colWidth
    marginRight (px 10)
  ".matrix-row-value" ? do
    display inlineBlock
    width colWidth
    marginRight (px 10)
  ".matrix-row" ** "label" ? do
    display inline
  ".matrix-row-inner" ? do
    margin (px 0) auto (px 0) auto
  ".matrix-col-label" ? do
    display inlineGrid
    minWidth colWidth
    marginRight (px 10)
  ".matrix-row-label" ? do
    display inlineGrid
    minWidth colWidth
    marginRight (px 10)
  ".validation-fail" ? do
    important $ borderColor red
