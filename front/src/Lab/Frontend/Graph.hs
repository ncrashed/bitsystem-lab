module Lab.Frontend.Graph(
    GraphConfig(..)
  , previewGraphCanvas
  , Layered(..)
  , onMouseSelect
  , getSelected2
  , setSelected
  , layeredGraphCanvas
  , initGraphPositions
  , getWorldGraph
  , Entity(..)
  ) where

import Apecs
import Apecs.Core
import Control.Concurrent
import Control.Monad
import Control.Monad.IO.Class
import Data.Bifunctor
import Data.Default
import Data.Graphics.Pixi.App
import Data.Graphics.Pixi.Graphics
import Data.Graphics.Pixi.Interaction
import Data.IORef
import Data.Typeable
import Language.Javascript.JSaddle
import Reflex.Dom

import Lab.Frontend.Graph.Render
import Lab.Frontend.Graph.System
import Lab.Frontend.Graph.Types
import Lab.Graph

import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as VU

data GraphConfig n e = GraphConfig {
  graphCfgCanvasWidth  :: !Int
, graphCfgCanvasHeight :: !Int
, graphCfgElement      :: RawElement GhcjsDomSpace
, graphCfgNodeSize     :: !Float
, graphCfgOnMouse      :: !(MouseEvent -> GraphSys n e ())
, graphCfgOnTick       :: !(GraphSys n e ())
, graphCfgOnInit       :: !(GraphSys n e ())
, graphCfgScale        :: !(V2 Float)
}

instance (Typeable n, Typeable e, Show n) => Default (GraphConfig n e) where
  def = GraphConfig {
      graphCfgCanvasWidth = 800
    , graphCfgCanvasHeight = 600
    , graphCfgElement = error "GraphConfig: element is not set"
    , graphCfgNodeSize = defNodeRadius
    , graphCfgOnMouse = onMousePick
    , graphCfgOnTick = applySelectedForce
    , graphCfgOnInit = pure ()
    , graphCfgScale = V2 1 1
    }

initGraphPositions :: MonadIO m => Int -> Int -> Graph (EdgeNode a ()) -> m (Graph (EdgeNode a (V2 Float)))
initGraphPositions canvasWidth canvasHeight = traverse $ \en -> case en of
  JustEdge a -> pure $ JustEdge a
  JustNode _ -> JustNode <$> genNodePosition d
  where
    d = V2 canvasWidth canvasHeight

-- | Get current world graph to pass around and draw in another canvas (or store it as file)
getWorldGraph :: forall n e m . (MonadJSM m, Show e, Typeable e, Typeable n, Integral e) => World n e -> m (Graph (EdgeNode e (n, V2 Float)))
getWorldGraph w = liftJSM $ runWith w $ do
  nodes <- getAllNodes
  edges <- getAllEdges
  pure $ Graph nodes edges
  where
    getAllNodes :: GraphSys n e (V.Vector (EdgeNode e (n, V2 Float)))
    getAllNodes = do
      s :: Storage (Node n) <- getStore
      es <- explMembers s
      V.forM (V.convert es) $ \i1 -> do
        (Pos p, Node{..})  <- get $ Entity i1
        pure $ JustNode (nodeState, p)

    getAllEdges :: GraphSys n e (V.Vector (GraphEdge (EdgeNode e (n, V2 Float))))
    getAllEdges = do
      s :: Storage (Edge e) <- getStore
      es <- explMembers s
      V.forM (V.convert es) $ \i1 -> do
        Edge e1 e2 _ a _ _ <- get $ Entity i1
        pure $ GraphEdge (unEntity e1) (unEntity e2) (JustEdge a)

data Layered a = Layered !a !Int

instance Show a => Show (Layered a) where
  show (Layered a i) = show a ++ "(" ++ show i ++ ")"

layeredGraphCanvas :: (MonadJSM m, Show a, Typeable a, Integral a) => GraphConfig (Layered Int) a -> Graph (EdgeNode a (Int, V2 Float)) -> m (World (Layered Int) a)
layeredGraphCanvas cfg = graphCanvas cfg . imapNodes (\i (n, p) -> (Layered (i+1) n, p))

previewGraphCanvas :: (MonadJSM m, Show a, Typeable a, Integral a) => GraphConfig Int a -> Graph (EdgeNode a (V2 Float)) -> m (World Int a)
previewGraphCanvas cfg = graphCanvas cfg . imapNodes (\i a -> (i+1, a))

graphCanvas :: forall e n m . (MonadJSM m, Show e, Show n, Typeable e, Typeable n, Integral e) => GraphConfig n e -> Graph (EdgeNode e (n, V2 Float)) -> m (World n e)
graphCanvas GraphConfig{..} g = do
  app <- pixiNewApp graphCfgCanvasWidth graphCfgCanvasHeight 0xFFFFFF
  let V2 sw sh = graphCfgScale
  pixiSetScale app sw sh

  e <- liftJSM $ toJSVal graphCfgElement
  pixiAppendTo app e
  pixiStageInteractive app True

  w <- newWorld
  liftJSM . runWith w $ do
    setDimensions graphCfgCanvasWidth graphCfgCanvasHeight
    es <- fmap V.fromList $ traverse (uncurry $ genNode graphCfgOnMouse app graphCfgNodeSize) $ getGraphNodes g
    mapM_ (makeEdge app es) (getGraphEdges g)
    graphCfgOnInit

  tickRef <- liftIO $ newIORef True
  void . liftIO . forkIO $ forever $ do
    threadDelay $ 1 * 1000000
    writeIORef tickRef True

  im <- getPixiInteraction app

  pixiAddTicker app $ runWith w $ do
    applyEdgesForce
    applyNodesForce
    -- applyNodesEdgesForce
    graphCfgOnTick
    applyVels
    centrifyAll
    drawEdges
    drawNodes

    mx <- getMouseX im
    my <- getMouseY im
    setMouse mx my

    isTick <- liftIO $ readIORef tickRef
    when isTick $ do
      -- evolveNodes
      liftIO $ writeIORef tickRef False
  pure w
