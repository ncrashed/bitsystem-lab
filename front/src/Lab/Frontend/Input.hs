{-# LANGUAGE OverloadedLists #-}
module Lab.Frontend.Input(
    inputField
  , matrixInput
  , matrixDisplay
  ) where

import Control.Lens
import Data.Foldable (traverse_)
import Data.Maybe
import Lab.Frontend.Monad
import Lab.Matrix
import Text.Read (readMaybe)

import qualified Data.Map.Strict as M
import qualified Data.Text as T
import qualified Data.Vector as V

inputField :: (Show a, Read a) => Text -> Dynamic t a -> Front t m (Dynamic t a)
inputField lbl da = do
  i <- genId
  labelFor i $ text lbl
  let cfg = def & inputElementConfig_elementConfig . elementConfig_initialAttributes .~ [("id", i)]
  rest <- dynamicTextInput cfg (showt <$> da)
  let (errE, resE) = splitMay . updated $ readMaybe . unpack <$> rest
  errorDiv $ fmap ("Неправильный формат" <$) errE
  a0 <- sampleDyn da
  holdDyn a0 resE

valueInputMaybe :: forall t m a er . (Show a, Read a) => InputElementConfig er t GhcjsDomSpace -> Dynamic t (Maybe a) -> Front t m (Dynamic t (Maybe a))
valueInputMaybe cfg da = mdo
  let cfg = def & inputElementConfig_elementConfig . elementConfig_modifyAttributes .~ errAttrE
  rest <- dynamicMTextInput cfg (fmap showt <$> da)
  let resE :: Event t (Maybe (Maybe a))
      resE = fmap (readMaybe . unpack) <$> updated rest
      valE :: Event t (Maybe a)
      valE = fforMaybe resE $ \case
        Nothing -> Just Nothing
        Just Nothing -> Nothing
        Just (Just a) -> Just (Just a)
      errE = ffor resE $ \case
        Just Nothing -> Just ()
        _ -> Nothing
      errAttrE = ffor errE $ \case
        Nothing -> [("class", Nothing)]
        Just _ -> [("class", Just "validation-fail")]
  a0 <- sampleDyn da
  holdDyn a0 valE

dynamicTextInput :: InputElementConfig er t GhcjsDomSpace -> Dynamic t Text -> Front t m (Dynamic t Text)
dynamicTextInput cfg dt  = do
  t0 <- sampleDyn dt
  fmap _inputElement_value $ inputElement $ cfg
    & inputElementConfig_initialValue .~ t0
    & inputElementConfig_setValue .~ updated dt
    & inputElementConfig_elementConfig . elementConfig_initialAttributes %~ M.insert "type" "text"

dynamicMTextInput :: InputElementConfig er t GhcjsDomSpace -> Dynamic t (Maybe Text) -> Front t m (Dynamic t (Maybe Text))
dynamicMTextInput cfg dt  = do
  t0 <- sampleDyn dt
  let wrap t = if T.null $ T.strip t then Nothing else Just t
  fmap (fmap wrap . _inputElement_value) $ inputElement $ cfg
    & inputElementConfig_initialValue .~ fromMaybe "" t0
    & inputElementConfig_setValue .~ (fromMaybe "" <$> updated dt)
    & inputElementConfig_elementConfig . elementConfig_initialAttributes %~ M.insert "type" "text"

matrixInput :: (Show a, Read a) => Dynamic t (Matrix (Maybe a)) -> Front t m (Dynamic t (Matrix (Maybe a)))
matrixInput md = fmap join $ divClass "matrix-input" $ widgetHoldDyn $ ffor md $ \m -> do
  let rowWrap = divClass "row matrix-row" . divClass "matrix-row-inner"
  rowWrap $ do
    divClass "matrix-row-label" $ label $ text ""
    traverse_ (divClass "matrix-col-label" . label . text . showt) ([1 .. matrixColumns m] :: [Int])
  res <- forMRows m $ \i r -> rowWrap $ do
    divClass "matrix-row-label" $ label . text . showt $ i+1
    V.forM r $ \ma -> valueInputMaybe def (pure ma)
  pure $ sequence res

matrixDisplay :: (Show a) => Dynamic t (Matrix (Maybe a)) -> Front t m ()
matrixDisplay md = void $ divClass "matrix-display" $ widgetHoldDyn $ ffor md $ \m -> do
  let rowWrap = divClass "row matrix-row" . divClass "matrix-row-inner"
  rowWrap $ do
    divClass "matrix-row-label" $ label $ text ""
    traverse_ (divClass "matrix-col-label" . label . text . showt) ([1 .. matrixColumns m] :: [Int])
  void $ forMRows m $ \i r -> rowWrap $ do
    divClass "matrix-row-label" $ label . text . showt $ i+1
    V.forM r $ \ma -> elClass "div" "matrix-row-value" $ maybe (text " ") (text . showt) ma
