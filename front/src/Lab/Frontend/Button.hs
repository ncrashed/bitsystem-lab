module Lab.Frontend.Button(
    outlineButton
  ) where

import Reflex.Dom

outlineButton :: DomBuilder t m => m a -> m (Event t a)
outlineButton ma = do
  (et, a) <- elClass' "button" "button button-outline" ma
  pure $ a <$ domEvent Click et
