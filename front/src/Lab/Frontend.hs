module Lab.Frontend(
    frontend
  , runFront
  ) where

import Data.Foldable (traverse_)
import Lab.Shared
import Reflex.Dom

import Lab.Frontend.Button
import Lab.Frontend.Graph
import Lab.Frontend.Input
import Lab.Frontend.Monad
import Lab.Frontend.Utils
import Lab.Graph
import Lab.Matrix
import Linear hiding (el)

import qualified Data.Vector as V
import qualified Data.List.NonEmpty as NE

import JSDOM.Types hiding (Text, Event)

makeDefaultMatrix :: Int -> Int -> Maybe Int
makeDefaultMatrix i1 i2 = case (i1+1, i2+1) of
  (1, 9) -> Just 2
  (2, 17) -> Just 2
  (3, 5) -> Just 4
  (3, 7) -> Just 5
  (3, 11) -> Just 3
  (4, 9) -> Just 2
  (5, 4) -> Just 3
  (5, 6) -> Just 4
  (6, 12) -> Just 3
  (7, 1) -> Just 2
  (7, 4) -> Just 2
  (8, 14) -> Just 2
  (9, 16) -> Just 3
  (10, 15) -> Just 1
  (11, 1) -> Just 3
  (11, 2) -> Just 4
  (12, 16) -> Just 2
  (13, 15) -> Just 2
  (14, 15) -> Just 2
  (16, 10) -> Just 3
  (16, 13) -> Just 3
  (17, 8) -> Just 2
  -- by task variation
  (12, 8) -> Just 4
  (9, 10) -> Just 4
  _ -> Nothing

-- | Task graph is graph with integer weights at edges and positions of nodes
type TaskGraph = Graph (EdgeNode Int (V2 Float))

-- | Graph that is indexed also by layer
type LayerGraph = Graph (EdgeNode Int (Int, V2 Float))

canvasWidth, canvasHeight :: Int
canvasWidth = 1500
canvasHeight = 900

data GraphBtn = SortBtn | PathBtn

frontend :: Front t m ()
frontend = void . retractStack $ container $ do
  dt <- form $ fieldset $ inputField "Количество узлов" $ pure 17
  let inMatrixD = ffor dt $ \n -> makeMatrix n n makeDefaultMatrix
  outMatrixD <- matrixInput inMatrixD
  graphD <- matrixToGraph outMatrixD
  void $ widgetHoldDyn $ ffor graphD $ \g -> void $ workflow (previewStage g)
  where
    previewStage g = Workflow $ do
      el "h3" $ text "Распределите вершины графа мышкой"
      sortE <- fmap (SortBtn <$) $ outlineButton $ text "Разбить на уровни"
      pathE <- fmap (PathBtn <$) $ outlineButton $ text "Поиск пути"
      graphE <- drawGraph g $ leftmost [sortE, pathE]
      let nextE = ffor graphE $ \(e, gr) -> case e of
            SortBtn -> layersStage gr $ layerGraph gr
            PathBtn -> pathStageFirst gr
      pure ((), nextE)
    layersStage origG Nothing = Workflow $ do
      el "h3" $ text "Граф разбит на уровни вершина(уровень)"
      btnE <- outlineButton $ text "Назад"
      divClass "error" $ text "Граф невозможно разбить на уровни"
      pure ((), previewStage origG <$ btnE)
    layersStage origG (Just g) = Workflow $ do
      el "h3" $ text "Граф разбит на уровни вершина(уровень)"
      btnE <- outlineButton $ text "Назад"
      grahpE <- drawLayerGraph g btnE
      pure ((), previewStage . unlayerGraph . snd <$> grahpE)
    pathStageFirst g = Workflow $ do
      el "h3" $ text "Выберите две вершины графа"
      btnE <- outlineButton $ text "Назад"
      (respE, selE) <- drawGraphSelect g btnE
      let nextE = leftmost [
              previewStage . snd <$> respE
            , (\(starti, endi, g) -> pathStageShow starti endi g) <$> selE
            ]
      pure ((), nextE)
    pathStageShow starti endi g = Workflow $ do
      el "h3" $ text "Результат алгоритма Флойда-Ху"
      btnE <- outlineButton $ text "Назад"
      let (mpath, dist, next) = floydWarshall starti endi g
      graphE <- case mpath of
        Nothing -> do
          divClass "error" $ text "Путь не найден"
          drawGraph g btnE
        Just p -> drawGraphPath g p btnE
      el "h4" $ text "Матрица расстояний"
      matrixDisplay $ pure dist
      el "h4" $ text "Матрица переходов"
      matrixDisplay $ pure $ fmap (fmap (+1)) next
      pure ((), pathStageFirst . snd <$> graphE)

matrixToGraph :: Dynamic t (Matrix (Maybe Int)) -> Front t m (Dynamic t TaskGraph)
matrixToGraph matD = do
  let rawGraphD = ffor matD $ \m -> fromMatrix (V.replicate (matrixRows m) ()) m
  performDynamic emptyGraph $ initGraphPositions canvasWidth canvasHeight <$> rawGraphD

drawGraph :: TaskGraph -> Event t a -> Front t m (Event t ((a, TaskGraph)))
drawGraph g reqE = do
  (elmnt, _) <- el' "div" $ pure ()
  let cfg = def {
          graphCfgCanvasWidth = canvasWidth
        , graphCfgCanvasHeight = canvasHeight
        , graphCfgElement = _element_raw $ elmnt
        }
  w <- previewGraphCanvas cfg g
  performEvent $ ffor reqE $ \a -> do
    g <- getWorldGraph w
    pure (a, mapNodes snd g)

drawLayerGraph :: forall a t m . LayerGraph -> Event t a -> Front t m (Event t ((a, LayerGraph)))
drawLayerGraph g reqE = do
  (elmnt, _) <- el' "div" $ pure ()
  let cfg = (def :: GraphConfig (Layered Int) Int) {
          graphCfgCanvasWidth = canvasWidth
        , graphCfgCanvasHeight = canvasHeight
        , graphCfgElement = _element_raw $ elmnt
        , graphCfgNodeSize = 1.5 * graphCfgNodeSize (def :: GraphConfig (Layered Int) Int)
        }
  w <- layeredGraphCanvas cfg g
  performEvent $ ffor reqE $ \a -> do
    g <- getWorldGraph w
    pure (a, mapNodes (\(Layered _ i, p) -> (i,p)) g)

drawGraphSelect :: TaskGraph -> Event t a -> Front t m (Event t ((a, TaskGraph)), Event t (NodeId, NodeId, TaskGraph))
drawGraphSelect g reqE = do
  (selE, selTrigger) <- newTriggerEvent
  (elmnt, _) <- el' "div" $ pure ()
  let cfg = (def :: GraphConfig Int Int) {
          graphCfgCanvasWidth = canvasWidth
        , graphCfgCanvasHeight = canvasHeight
        , graphCfgElement = _element_raw $ elmnt
        , graphCfgOnMouse = onMouseSelect
        , graphCfgOnTick = do
            msel <- getSelected2
            flip traverse_ msel $ \v -> do
              liftIO $ selTrigger v
              setSelected []
        }
  w <- previewGraphCanvas cfg g
  respE <- performEvent $ ffor reqE $ \a -> do
    g <- getWorldGraph w
    pure (a, mapNodes snd g)
  selectedE' <- performEvent $ ffor selE $ \(Entity e1, Entity e2) -> do
    g <- getWorldGraph w
    pure (e1, e2, mapNodes snd g)
  selectedE <- delay 0.1 selectedE'
  pure (respE, selectedE)

drawGraphPath :: TaskGraph -> Path Int (V2 Float) -> Event t a -> Front t m (Event t ((a, TaskGraph)))
drawGraphPath g p reqE = do
  (elmnt, _) <- el' "div" $ pure ()
  let cfg = (def :: GraphConfig Int Int) {
          graphCfgCanvasWidth = canvasWidth
        , graphCfgCanvasHeight = canvasHeight
        , graphCfgElement = _element_raw $ elmnt
        , graphCfgOnMouse = const $ pure ()
        , graphCfgOnTick = pure ()
        , graphCfgOnInit = do
            let pes = Entity . pathStepNodeId <$> NE.toList p
            setSelected pes
        }
  w <- previewGraphCanvas cfg g
  performEvent $ ffor reqE $ \a -> do
    g <- getWorldGraph w
    pure (a, mapNodes snd g)
