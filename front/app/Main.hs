module Main where

import Lab.Frontend
import Lab.Frontend.Style
import Reflex.Dom

main :: IO ()
main = mainWidgetWithCss frontendCssBS $ runFront frontend
