module Main where

import Lab.Backend
import Data.Semigroup ((<>))
import Network.Wai.Handler.Warp
import Options.Applicative

data Options = Options {
  staticsPath :: FilePath
, listenPort  :: Port
}

options :: Parser Options
options = Options
  <$> strOption
      (  long "statics"
      <> metavar "STATICS_PATH"
      <> help "Folder with static HTML/CSS/JS to serve."
      <> value "../front/statics"
      <> showDefault )
  <*> option auto
      (  long "port"
      <> metavar "PORT_NUMBER"
      <> help "Which port the server listens."
      <> value 8080
      <> showDefault  )

main :: IO ()
main = app =<< execParser opts
  where
    opts = info (options <**> helper)
       ( fullDesc
      <> progDesc "Example of application backend server for reflex-dom"
      <> header "lab-back - an example for reflex-dom application" )

app :: Options -> IO ()
app Options{..} = do
  putStrLn $ "Started listening on 127.0.0.1:" ++ show listenPort
  putStrLn $ "Open the URL: http://127.0.0.1:" ++ show listenPort ++ "/index.html"
  run listenPort $ staticsApp staticsPath
